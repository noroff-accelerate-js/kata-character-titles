const uppercaseFirstLetter = (word) => word.charAt(0).toUpperCase() + word.slice(1)

const correctTitle = (title) => {
    // Add space after comma
    title = title.replace(/,(\S)/, ", $1")

    // Split string into lowercase words
    let words = title.split(" ").map(word => word.toLowerCase())
    
    // Exceptions for uppercase
    const exceptions = ["and", "the", "of", "in"]

    words = words.map(word => exceptions.includes(word) ? word : uppercaseFirstLetter(word))

    return words.join(" ")
}

console.log("correctTitle(\"jOn SnoW, kINg IN thE noRth\")", "➞", correctTitle("jOn SnoW, kINg IN thE noRth"))
console.log("correctTitle(\"sansa stark,lady of winterfell.\")", "➞", correctTitle("sansa stark,lady of winterfell."))